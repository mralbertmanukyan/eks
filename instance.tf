provider "aws" {
}

resource "aws_key_pair" "mykey" {
  key_name= "id_rsa.pub"
  public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCoxzJkztUAMcqYfocAO2p3yDUEQ0DjAgKEE+Vxb/O2o/HnqUAy9NZwVIqZmGd+rInGTLdNPztXhUXHGITQkTAwyT/20ilKD1Pp2MbXbD3Z7EVFNgVxdrwvoaITWsWmpZJpJ/VrGXwdNseQ6bxsbxaui3Xt/VdBeOvQor+z82lFm3jxD3Ia9qeZ6+tqHunC0Er49XC25abn+qtACg+AjMufKUA8w9W/xuOOlusXl/zKPhbxnvShbvOy1i3q7JOVldST7tfXODkT/NtT4RYtbqjix0ofXs0/gkl8K+zEe2YWhXd/FSEgfJ8cEFqEZnD6SFYIxxadf1ubVaxEm2rFb3C5"
}

resource "aws_instance" "django1" {
  ami = "ami-0de9f803fcac87f46"
  instance_type = "t2.micro"
  key_name = "id_rsa.pub"
    tags = {
      Name = "django1"
  }

  provisioner "file" {
   source = "/builds/mralbertmanukyan/eks/install_django.sh"
   destination = "/tmp/install_django.sh"
  }
  provisioner "remote-exec" {
    inline = [
     "chmod +x /tmp/install_django.sh",
     "sudo /tmp/install_django.sh"
    ]
  }

  connection {
    host = coalesce(self.public_ip)
    type = "ssh"
    user = "ec2-user"
    private_key = file("/opt/id_rsa")
  }
}
