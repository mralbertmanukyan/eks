FROM alpine
RUN apk add --no-cache \
jq \
bash \
unzip \
python3 \
py3-pip \
&& pip3 install --upgrade pip \
&& pip3 install \
awscli \
&& rm -rf /var/cache/apk/*
RUN wget https://releases.hashicorp.com/terraform/0.14.8/terraform_0.14.8_linux_amd64.zip
RUN unzip terraform_0.14.8_linux_amd64.zip && rm terraform_0.14.8_linux_amd64.zip
RUN mv terraform /usr/bin/terraform
CMD bash
