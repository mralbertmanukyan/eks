#!/bin/bash

wget https://kojipkgs.fedoraproject.org//packages/sqlite/3.8.11/1.fc21/x86_64/sqlite-devel-3.8.11-1.fc21.x86_64.rpm

wget https://kojipkgs.fedoraproject.org//packages/sqlite/3.8.11/1.fc21/x86_64/sqlite-3.8.11-1.fc21.x86_64.rpm

sudo yum install sqlite-3.8.11-1.fc21.x86_64.rpm sqlite-devel-3.8.11-1.fc21.x86_64.rpm gcc python36 python3-pip -y

sudo pip3 install django

sudo cp /usr/local/bin/django-admin /usr/bin/

cd /opt

sudo django-admin startproject aws_django

cd aws_django

sudo python3 manage.py migrate

cd

sudo mv /opt/aws_django/aws_django/settings.py .

sudo sed -i 's/ALLOWED_HOSTS.*/ALLOWED_HOSTS = ["'*'"]/g' settings.py

sudo mv settings.py  /opt/aws_django/aws_django/

cd /opt/aws_django/ && sudo python3 manage.py runserver 0.0.0.0:8000 &
